@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-3 p-5">
            <img class="rounded-circle" src="https://instagram.frak1-1.fna.fbcdn.net/vp/eee7e5f164969f7ca06cc132c6259edb/5E4E38D2/t51.2885-19/s150x150/72898287_2961618410516015_8665959726502117376_n.jpg?_nc_ht=instagram.frak1-1.fna.fbcdn.net" alt="">
        </div>
        <div class="col-9 pt-5">
            <div>
                <div class="d-flex justify-content-between align">
                    <h3>{{ $user->username }}</h3>
                    <a href="/p/create">New Post !</a>
                </div>
                <div class="d-flex">
                    <div class="pr-4"><strong>{{ $user->posts->count() }}</strong> Posts</div>
                    <div class="pr-4"><strong>99k</strong> Followers</div>
                    <div class="pr-4"><strong>90</strong> Following </div>
                </div>
                <div class="pt-3" ><h5>{{ $user->profile->title }}</h5></div>
                <div>{{ $user->profile->description }}</div>
                <div><strong><a href="#">{{ $user->profile->url }}</a></strong></div>
            </div>
        </div>
    </div>
    <div class="row" class="pt-3">
    @foreach( $user->posts as $post )
        <div class="col-4 pb-5">
            <a href="/p/{{ $post->id }}">
                <img src="/storage/{{ $post->image }}" alt="" class="w-100">
            </a>
        </div>
    @endforeach
    </div>
</div>
@endsection
